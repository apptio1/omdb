FROM golang:alpine as builder

ENV GO111MODULE=on 
RUN mkdir /build 
ADD . /build/
WORKDIR /build
RUN go get ./...
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -ldflags '-extldflags "-static"' -o main .

FROM scratch
COPY --from=builder /build/main /app/
COPY --from=builder /build/config.yaml /app/
WORKDIR /app
ENTRYPOINT ["./main"]
