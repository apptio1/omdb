package main

import (
	"testing"
)

func TestIsValidTitle(t *testing.T) {
	var omdb Omdb

	for _, tc := range titleTestCases {
		omdb.title = tc.title
		testResponse := omdb.isValidTitle()
		if testResponse == tc.expected {
			t.Log("PASS : " + tc.description)
		}
	}
	t.Log("Tested ", len(titleTestCases), " test cases")
}

func TestIsValidCType(t *testing.T) {
	var omdb Omdb

	for _, tc := range cTypeTestCases {
		omdb.ctype = tc.cType
		testResponse := omdb.isValidCType()
		if testResponse == tc.expected {
			t.Log("PASS : " + tc.description)
		}
	}
	t.Log("Tested ", len(cTypeTestCases), " test cases")
}

func TestIsValidYear(t *testing.T) {
	var omdb Omdb

	for _, tc := range yearTestCases {
		omdb.year = tc.year
		testResponse := omdb.isValidCType()
		if testResponse == tc.expected {
			t.Log("PASS : " + tc.description)
		}
	}
	t.Log("Tested ", len(yearTestCases), " test cases")
}

func TestIsValidPlot(t *testing.T) {
	var omdb Omdb

	for _, tc := range plotTestCases {
		omdb.plot = tc.plot
		testResponse := omdb.isValidPlot()
		if testResponse == tc.expected {
			t.Log("PASS : " + tc.description)
		}
	}
	t.Log("Tested ", len(plotTestCases), " test cases")
}
