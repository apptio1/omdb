[![pipeline status](https://gitlab.com/apptio1/omdb/badges/master/pipeline.svg)](https://gitlab.com/apptio1/omdb/-/commits/master) [![coverage report](https://gitlab.com/apptio1/omdb/badges/master/coverage.svg)](https://gitlab.com/apptio1/omdb/-/commits/master)
# Desctiption
This project implements CLI based search for Online Movie Data Base (OMDB).

# Usage

```yaml
Searches omdb api...
  -p string
    [short|full]
  -ratingSource showRatings
    Which source to show the ratings from (Valid if showRatings is selected.) (default "Rotten Tomatoes")
  -showRatings
    Shows ratings from Rotten Tomatoes
  -t string
    Movie title to search for.
  -type string
    Type of title [movie|series|episodes]
  -url string
    apiUrl for omdb (default "http://www.omdbapi.com/")
  -y int
    Year of release.
```


# Example Run

## Docker

Download latest container from [gitlab](registry.gitlab.com/apptio1/omdb)

```bash
docker run --rm \
--env OMDB_API_KEY=${OMDB_API_KEY} \
--rm registry.gitlab.com/apptio1/omdb \
\-t Planet
```
**NOTE**:
1. The command strings would need to be escaped
2. [OMDB_API_KEY](https://www.omdbapi.com/apikey.aspx?__EVENTTARGET=freeAcct&__EVENTARGUMENT=&__LASTFOCUS=&__VIEWSTATE=%2FwEPDwUKLTIwNDY4MTIzNQ9kFgYCAQ9kFgICBw8WAh4HVmlzaWJsZWhkAgIPFgIfAGhkAgMPFgIfAGhkGAEFHl9fQ29udHJvbHNSZXF1aXJlUG9zdEJhY2tLZXlfXxYDBQtwYXRyZW9uQWNjdAUIZnJlZUFjY3QFCGZyZWVBY2N0x0euvR%2FzVv1jLU3mGetH4R3kWtYKWACCaYcfoP1IY8g%3D&__VIEWSTATEGENERATOR=5E550F58&__EVENTVALIDATION=%2FwEdAAU5GG7XylwYou%2BzznFv7FbZmSzhXfnlWWVdWIamVouVTzfZJuQDpLVS6HZFWq5fYpioiDjxFjSdCQfbG0SWduXFd8BcWGH1ot0k0SO7CfuulN6vYN8IikxxqwtGWTciOwQ4e4xie4N992dlfbpyqd1D&at=freeAcct&Email=) needs to be set as an environment variable.

## CLI
Download latest binary from [gitlab releases](https://gitlab.com/apptio1/omdb/-/releases)

```bash
export OMDB_API_KEY=<api-key>
./omdb -t Planet
```
**NOTE**: For *nix binaries you will need to give execution permissions via command: `chmod +x ./omdb`

## Show Ratings (Docker)
```bash
docker run --rm \
--env OMDB_API_KEY=${OMDB_API_KEY} \
--rm registry.gitlab.com/apptio1/omdb \
\-t Planet \
\-showRatings
```

## Show Ratings (CLI)
```bash
export OMDB_API_KEY=<api-key>
./omdb -t Planet -showRatings
```
