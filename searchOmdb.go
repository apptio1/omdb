/*
Online Movie Data Base (OMDB) provides details of all the movies with their ratings. This CLI will enable us to 
search the omdb database and present the output in human readable form to the user.
*/
package main

import (
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"regexp"
	"strconv"
	"strings"

	"gopkg.in/yaml.v2"
)

// ### variables ##
// project constants
const CONFIG_FILE_NAME = "config.yaml"
const ENV_OMDB_API_KEY_VAR_NAME = "OMDB_API_KEY"
const ENV_OMDB_API_URL = "OMDB_API_URL"
const VALID_CTYPES = "movie,movies,serie,series,episode,episodes"

// structs
// variables required for omdb search
type Omdb struct {
	title        string
	ctype        string
	year         int
	plot         string
	showRatings  bool
	ratingSource string
}

// variables required for configuring the search api
type Config struct {
	apiKey string
	apiUrl string
}

type ConfigYaml struct {
	TitleType []string `yaml:"titleType"`
	ApiUrl    string   `yaml:"apiURL"`
	Defaults  struct {
		TitleType    string `yaml:"titleType"`
		RatingSource string `yaml:"ratingSource"`
	}
}

type OmdbResponse struct {
	Title    string `json:"Title"`
	Year     string `json:"Year"`
	Rated    string `json:"Rated"`
	Error    string `json:"Error"`
	Released string `json:"Released"`
	Runtime  string `json:"Runtime"`
	Genre    string `json:"Genre"`
	Director string `json:"Director"`
	Writer   string `json:"Writer"`
	Actors   string `json:"Actors"`
	Plot     string `json:"Plot"`
	Language string `json:"Language"`
	Country  string `json:"Country"`
	Awards   string `json:"Awards"`
	Poster   string `json:"Poster"`
	Ratings  []struct {
		Source string `json:"Source"`
		Value  string `json:"Value"`
	} `json:"Ratings"`
	Metascore  string `json:"Metascore"`
	ImdbRating string `json:"imdbRating"`
	ImdbVotes  string `json:"imdbVotes"`
	ImdbID     string `json:"imdbID"`
	Type       string `json:"Type"`
	DVD        string `json:"DVD"`
	BoxOffice  string `json:"BoxOffice"`
	Production string `json:"Production"`
	Website    string `json:"Website"`
	Response   string `json:"Response"`
}

// ## main controller
func main() {
	// set logging
	// getting rid of all the prefixes(https://golang.org/pkg/log/#pkg-constants) - they dont make sense for a cli
	log.SetFlags(0)

	// variables
	var cfg Config
	var omdb Omdb
	var omdbRes OmdbResponse

	// load values from config file
	omdb = cfg.init(omdb)

	// fetch API_KEY from environment variable
	cfg.apiKey = os.Getenv(ENV_OMDB_API_KEY_VAR_NAME)
	if cfg.apiUrl == "" {
		cfg.apiUrl = os.Getenv(ENV_OMDB_API_URL)
	}

	//declaring flags for cli
	flag.Usage = func() {
		log.Println("Searches omdb api...")
		flag.PrintDefaults()
	}

	flag.StringVar(&omdb.title, "t", "", "Movie title to search for.")
	flag.StringVar(&omdb.ctype, "type", "", "Type of title [movie|series|episodes]")
	flag.IntVar(&omdb.year, "y", 0, "Year of release.")
	flag.StringVar(&omdb.plot, "p", "", "[short|full]")
	flag.BoolVar(&omdb.showRatings, "showRatings", false, "Shows ratings from Rotten Tomatoes")
	flag.StringVar(&omdb.ratingSource, "ratingSource", omdb.ratingSource, "Which source to show the ratings from (Valid if `showRatings` is selected.)")
	flag.StringVar(&cfg.apiUrl, "url", cfg.apiUrl, "apiUrl for omdb")

	flag.Parse()

	// validate cli input
	if !isValid(omdb, cfg) {
		flag.PrintDefaults()
		log.Fatalf("Validations failed..")
	}

	// check for response from
	omdbRes, err := cfg.httpReqOmdb(omdb)
	if err != nil {
		log.Fatal(err)
	}

	err = omdbRes.printSearchOutput(omdb)
	if err != nil {
		log.Fatal(err)
	}

}

// load values from config file
func (cfg *Config) init(omdb Omdb) Omdb {
	var yml ConfigYaml
	ymlFile, err := ioutil.ReadFile(CONFIG_FILE_NAME)
	if err != nil {
		log.Printf("yamlFile.Get err:  #%v", err)
		log.Printf("Unable to load file %v. ", CONFIG_FILE_NAME)
	}

	err = yaml.Unmarshal(ymlFile, &yml)
	if err != nil {
		log.Fatalf("Unmarshal err: %v", err)
	}

	// set values
	cfg.apiUrl = yml.ApiUrl
	omdb.ratingSource = yml.Defaults.RatingSource
	return omdb
}

// validations
func isValid(omdb Omdb, cfg Config) bool {
	//stripping whitespaces
	omdb.title = strings.TrimSpace(omdb.title)
	omdb.ctype = strings.TrimSpace(omdb.ctype)
	omdb.plot = strings.TrimSpace(omdb.plot)
	cfg.apiUrl = strings.TrimSpace(cfg.apiUrl)
	cfg.apiKey = strings.TrimSpace(cfg.apiKey)

	// validations
	if !omdb.isValidTitle() {
		return false
	}

	if !omdb.isValidCType() {
		return false
	}

	if !omdb.isValidYear() {
		return false
	}

	if !omdb.isValidPlot() {
		return false
	}

	if !cfg.isValidApiKey() {
		return false
	}

	if !cfg.isValidApiUrl() {
		return false
	}

	return true
}

// validate movie title
func (omdb Omdb) isValidTitle() bool {
	//stripping whitespaces
	omdb.title = strings.TrimSpace(omdb.title)

	// title should be non empty
	if omdb.title == "" {
		log.Println("Title cannot be blank")
		return false
	} else if hasSymbol(omdb.title) {
		//movie title cshould not contain symbols (Ex. ∑´®†,etc.)
		log.Println("Title cannot contain symbols")
		return false
	} else if len(omdb.title) > 500 {
		//movie title should be <500 characters (refer https://www.imdb.com/list/ls064443882/)
		log.Fatal("Title cannot be longer than 500 characters")
		return false
	} else {
		return true
	}
}

//validate ctype
func (omdb Omdb) isValidCType() bool {
	// all possible ctypes
	var validCTypes = strings.Split(VALID_CTYPES, ",")

	// CTYPE is not a mandatory field - return true if null
	if omdb.ctype == "" {
		return true
	}

	// if ctype is present it should be amongst the valid ctypes
	ctypePresent := false
	for _, validCtype := range validCTypes {
		if strings.EqualFold(validCtype, omdb.ctype) {
			ctypePresent = true
		}
	}
	if !ctypePresent {
		log.Printf("Invalid value %v, Type should be one of %q", omdb.ctype, validCTypes)
		return false
	}
	return true
}

func (omdb Omdb) isValidYear() bool {
	// year is not mandatory
	if omdb.year == 0 {
		return true
	}
	// should match regex of [0-9]{4}
	match, err := regexp.MatchString(`[0-9]{4}`, strconv.Itoa(omdb.year))

	if err != nil {
		log.Fatal("regexp failed..")
	}

	if !match {
		log.Printf("Invalid year specified. Value shoould match regex [0-9]{4}")
		return false
	}

	if len(strconv.Itoa(omdb.year)) != 4 {
		log.Printf("Invalid year specified. Value should be 4 digits")
		return false
	}

	return true
}

func (omdb Omdb) isValidPlot() bool {
	// plot is not mandatory
	if omdb.plot == "" {
		return true
	}
	// plot should be one of short|full
	validPlot := "short,full"
	isValidPlot := false
	for _, plot := range strings.Split(validPlot, ",") {
		if strings.EqualFold(plot, omdb.plot) {
			isValidPlot = true
		}
	}
	if !isValidPlot {
		log.Printf("Invalid value %v, Plot should be one of %q,", omdb.plot, strings.Split(omdb.plot, ","))
		return false
	}
	return true
}

func (cfg Config) isValidApiKey() bool {
	// apiKey is mandatory
	if cfg.apiKey == "" {
		log.Printf("apiKey not specified. It needs to be set as an environment variable %v", ENV_OMDB_API_KEY_VAR_NAME)
		return false
	}
	// apikey should be<=10 chars and alpha-numeric
	if len(cfg.apiKey) > 10 {
		log.Printf("apiKey should be less than 10 chracters")
		return false
	}
	match, err := regexp.MatchString(`[a-zA-Z0-9]`, cfg.apiKey)
	if err != nil {
		log.Fatal("regexp Failed..")
	}

	if !match {
		log.Printf("Invalid apiKey. apiKey does not match the regexp [A-Za-z0-9]")
		return false
	}

	return true
}

func (cfg Config) isValidApiUrl() bool {
	// should not be blank and should meet url regex
	if cfg.apiUrl == "" {
		log.Printf("api-url cannot be blank.")
		return false
	} else if !isValidUrl(cfg.apiUrl) {
		log.Printf("Invalid api-url : %v", cfg.apiUrl)
		return false
	}

	return true
}

//helper functions
//https://golangcode.com/how-to-check-if-a-string-is-a-url/
func isValidUrl(uri string) bool {
	_, err := url.ParseRequestURI(uri)
	if err != nil {
		return false
	}

	parseUrl, err := url.Parse(uri)
	if err != nil || parseUrl.Scheme == "" || parseUrl.Host == "" {
		return false
	}

	return true
}

// checks if the given string has sympbols/special characters in it
// https://stackoverflow.com/a/31964846/6333264
func hasSymbol(str string) bool {
	f := func(r rune) bool {
		return r < 'A' || r > 'z'
	}
	if strings.IndexFunc(str, f) != -1 {
		return true
	}
	return false
}

// return the complete url with all the arguments
func (cfg Config) getOmdbRequestUrl(omdb Omdb) (string, error) {
	// local variables
	var url string
	// if the title is not set then we should err out
	if omdb.title == "" {
		return "", errors.New("Title is required!")
	} else {
		url = cfg.apiUrl + "?t=" + omdb.title
		if omdb.ctype != "" {
			url = url + "&type=" + omdb.ctype
		}
		if omdb.year != 0 {
			url = url + "&y=" + strconv.Itoa(omdb.year)
		}
		if omdb.plot != "" {
			url = url + "&plot=" + omdb.plot
		}
	}

	// append the url with api-token
	url = url + "&apikey=" + cfg.apiKey

	return url, nil
}

// implement rest calls
func (cfg Config) httpReqOmdb(omdb Omdb) (OmdbResponse, error) {
	// local variables
	var url string
	var data OmdbResponse

	url, err := cfg.getOmdbRequestUrl(omdb)
	if err != nil {
		return data, err
	}

	req, _ := http.NewRequest("GET", url, nil)
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Cache-Control", "no-cache")
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return data, err
	}

	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return data, err
	}

	json.Unmarshal(body, &data)

	// handle omdb error response
	if data.Error != "" {
		return data, errors.New("Invalid response from " + cfg.apiUrl + " : " + data.Error)
	}

	return data, nil
}

func (omdbRes OmdbResponse) printSearchOutput(omdb Omdb) error {
	// local variables
	var rating string
	var ratingSource string
	var err error
	// check if rotten tommatoe ratings is to be displayed
	if omdb.showRatings {
		rating, ratingSource, err = omdbRes.getRatings(omdb)
		if err != nil {
			return err
		}
	}

	log.Println("############################################################################")
	log.Printf("~~~ \t 	%v \t: %v ~~~", strings.ToUpper(omdbRes.Type), strings.ToUpper(omdbRes.Title))
	fmt.Println("############################################################################")
	fmt.Printf("Year      \t: %v\n", omdbRes.Year)
	fmt.Printf("Rated     \t: %v\n", omdbRes.Rated)
	fmt.Printf("Runtime   \t: %v\n", omdbRes.Runtime)
	fmt.Printf("Genre     \t: %v\n", omdbRes.Genre)
	if omdb.showRatings {
		fmt.Printf("Rating    \t: %v (%v)\n", rating, ratingSource)
	}
	fmt.Printf("Director  \t: %v\n", omdbRes.Director)
	fmt.Printf("Writer	  \t: %v\n", omdbRes.Writer)
	fmt.Printf("Actors	  \t: %v\n", omdbRes.Actors)
	fmt.Printf("Plot   	  \t: %v\n", omdbRes.Plot)
	fmt.Printf("Language  \t: %v\n", omdbRes.Language)
	fmt.Printf("Country   \t: %v\n", omdbRes.Country)
	fmt.Printf("Awards    \t: %v\n", omdbRes.Awards)
	fmt.Printf("Production\t: %v\n", omdbRes.Production)
	fmt.Printf("Website   \t: %v\n", omdbRes.Website)
	fmt.Printf("Response  \t: %v\n", omdbRes.Response)

	return err
}

func (omdbRes OmdbResponse) getRatings(omdb Omdb) (string, string, error) {
	// local variables
	rater := omdb.ratingSource

	for i := 0; i < len(omdbRes.Ratings); i++ {
		if omdbRes.Ratings[i].Source == rater {
			return omdbRes.Ratings[i].Value, omdbRes.Ratings[i].Source, nil
		}
	}
	return "", "", errors.New("RatingSource " + rater + " is not present from the source")
}
