# Section 2
- - Issue : https://github.com/terraform-providers/terraform-provider-aws/issues/13176
  - PR (WIP): https://github.com/terraform-providers/terraform-provider-aws/pull/13203
- Issue : https://github.com/linkerd/linkerd2/issues/4838

# Section 3
## 1.  What project have you found the most challenging in your career? Why?
License remediation for a major client. Oracle licensing exposed the client to multi-million dollar loss due to incompatability with the hypervisor - Oracle only considers `hard partitioning` in the [Solaris hyper-visor](http://www.oracle.com/us/corporate/pricing/partitioning-070609.pdf). As a aprt of this remediation I had to:
1. Create a list of all the Oracle software installations in middleware for the client which incl. 100x of servers with multiple Oracle Fusion Middleware softwares
2. Identify license remediation strategy
3. Move the softwares wherever possible to valid servers
4. Remediate the licenses
The project was challenging because of:
* visbility: CXO level visbility due to size of the exposure (multi-million dollar exposure)
* size: 100x of servers with multiple installtions of software
* multiple vendors: contacting hardware vendors for planning remediation work (network changes, hosting changes, etc)
* Oracle licensing: delibrately made licensing complicated and vague to render clients exposed

## 2.  What has been the project that you are the proudest? Why?
The same project above. I take pride in it because:
* licensing was something absolutely new to me.
* I was able to turn-around million-dollar exposure to 10x thousand $ exposure
* I was able to rectify wrong assumptions made earlier by the licensing managers and educate them
* The remediation was done in 2-3 weeks.

## 3.  When have you been the happiest in your professional career? Why?
While working for a consultancy to implement a ground-up Cloud Migration project which involved:
* Creating a landing zones in AWS for clients
* Migrating 5 windows based applications in AWS Cloud
* Implementing AWS Security best practices:
  * Create AWS Organisation and multiple AWS Account (IAM, Audit, Root/billing, prod-pci,prod,non-prod,non-prod-pci,etc)
  * Create Cloudtrail, Config, VPC logs
  * All configs should be KMS encrypted
  * Enable Lambda functions for operational stuff (tag reporting, snapshotting, etc.)
  * Sumologic dashboards for PCI compliance, CIS compliance,etc.
  * Microsoft patching using AWS RunCommands
  * Microsoft SQL Backups in EC2 using RunCommands
* Everything to be managed via Code
* Finish all the project deliverables in 2 months time

This was the happiest movement becuase:
* the project was challenging
* i had not done builds in MS and not used PowerShell - so got to learn and do something new
* The delivery in 2 months meant we had strict timelines and the epics were closely checked tracked
* i got to work with some really brilliant/intelligent techies
* i got recognized for my dedication and hard-work by the organisation

## 4.  When have you been the unhappiest in your professional career? Why?
For one of the consultancies I was involved in a project to migrate a Content Management System based application onto the cloud. The application was in PHP with lots of legacy dependencies. It was unhappiend moment because:
* Everything had to be done manually
* Dependencies were not known and not only am I a novice in PHP I hate it too
* Application was secured via `.htaccess` file which had rules from valid CIDR blocks that it whitelisted (some of which were no longer valid)
* Apache libraries were old and difficult to build
* Application build had to be reverse-engineered
* There was a lot of hard-work required but not much incentive since this project was a kick in the door for the client

Another similar similar project in which I felt disgusted was the one which involved me implementing a Lambda for performing automated tagging and implementing AWS Organsiation for a big enterprise. _The Enterprise_ however wanted me to implement this project in 6 months whereas my deliverables (Solution Architecture, Infra Code, Demo and required approvals ) were read in the first couple of weeks.
