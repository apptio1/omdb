## Resources: What external references/sources you used to arrive at the solution
* https://golang.org/doc/effective_go.html
* https://golang.org/pkg/flag/
* https://www.omdbapi.com/
* https://golangcode.com/how-to-check-if-a-string-is-a-url/
* https://mholt.github.io/json-to-go/
* http://networkbit.ch/golang-column-print/
* https://www.cloudreach.com/en/resources/blog/cts-build-golang-dockerfiles/
* https://oddcode.daveamit.com/2018/08/16/statically-compile-golang-binary/


## Design and Implementation: Design choices and decisions
### Choice 1 - Language
Decided to use golang due to convenience with implementing data-structures, capability to easily read standard configuration files, polymorphism.
### Choice 2 - Data structures
There are 4 _majorly_ different _kinds_ of data structure that have been identified based on usage patterns:
* DS for API request
* DS for configuration
* DS for response fields
* DS for fields that are to be loaded from Config file
### Choice 3 - Static unmarshalling
Json spec is not complicated and is predictable/static

## Alternatives: What alternatives did you consider? Why did you not choose those options?
### Bash
* Data structure implementation is not easy
* would be OS specific
* validation would be complicated due to inavailability of libraries like ignoreCase string comparison

### Python
* Would need a python installation for the cli to run; golang however once compiled and built can run agnostic of dependencies

### Dynamic Unmarshaling
Would be complicated and slightly more time consuming to implement since that will be a something I have not implemented before. It'll also need more test-cases to validate the reads of desired Json structures.

## Deployment Plan: How will this be deployed?
create a release for OSX, linux, windows and docker container via project releases in gitlab


## Monitoring/Reporting/Alerts: How will your team know that your program/solution is working as expected? What metrics/charts would you
monitor and report for this component?
* exit code
* execution time
* load time
* CPU time
* memory usage

## Configuration: What configuration parameters will this need? How will the administrators set these parameters?
* set defaults for command line
* set api-key from ENV-Variable
* set defaults from config-file `config.yaml`

## Testing: What is your test strategy?
* implement unit test (partially immplemented)
* monitor test cov (Not Implemented)
* performance test (Not Implemented)

