package main

var titleTestCases = []struct {
	title       string
	expected    bool
	description string
}{
	{
		"planet",
		true,
		"test valid title name",
	},
	{
		"",
		false,
		"test null value",
	},
	{
		"!@plane",
		false,
		"test special characters",
	},
}

var cTypeTestCases = []struct {
	cType       string
	expected    bool
	description string
}{
	{
		"movie",
		true,
		"test valid value",
	},
	{
		"episode",
		true,
		"test valid value",
	},
	{
		"@#$sdg",
		false,
		"test special characters",
	},
}

var yearTestCases = []struct {
	year        int
	expected    bool
	description string
}{
	{
		2010,
		true,
		"test valid value",
	},
	{
		2011,
		true,
		"test valid value",
	},
	{
		201201413091324,
		false,
		"test special characters",
	},
	{

		2022,
		false,
		"test year > today",
	},
}

var plotTestCases = []struct {
	plot        string
	expected    bool
	description string
}{
	{
		"short",
		true,
		"test value value",
	},
	{
		"full",
		true,
		"test valid value",
	},
	{
		"Full",
		true,
		"test valid value with mized case",
	},
	{
		"sHoRt",
		true,
		"test valid value with mixed case",
	},
	{
		"shortS",
		false,
		"test invalid value",
	},
}
